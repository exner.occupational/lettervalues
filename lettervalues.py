from itertools import permutations
from itertools import product


def get_all_permutations(fix):
    # Get all possible combinations of values from the dictionary
    value_combinations = product(*fix.values())

    # Filter combinations to exclude those where a value is used twice
    filtered_combinations = filter(lambda x: len(set(x)) == len(x), value_combinations)

    # Create a list of dictionaries with keys and corresponding values
    permutations = [{key: value for key, value in zip(fix.keys(), perm)} for perm in filtered_combinations]

    return permutations

def min_values_sum(word):
    # Step 1: Build a dict with letter as key and letter count as value
    wd = {}
    for letter in word:
        wd[letter] = wd.get(letter, 0) + 1
    # Step 2: Sort dictionary items by count in descending order
    sorted_wd = dict(sorted(wd.items(), key=lambda item: item[1], reverse=True))
    sorted_wd.popitem()
    # Step 3: Calculate the sum of products
    s = sum(count * (index + 1) for index, (letter, count) in enumerate(sorted_wd.items()))
    return s

def get_next_best(angabe, fix):
    """
    calc the best next word to process (i. e. the word with less new letters)
    :returns: word, new letters, estimated max value of letters to guess
    """
    fix_letters = set(fix.keys())
    result_dict = {}
    for word in angabe.keys():
        letters = set(word)
        letters.difference_update(fix_letters)
        if len(letters) > 0:
            #result_dict[word] = len(letters)
            #result_dict[word] = angabe[word]
            result_dict[word] = (len(letters), angabe[word])
    #print(f"result_dict: {result_dict}")
    # Find the key with the minimum value
    min_word = min(result_dict, key=lambda k: (result_dict[k][0], result_dict[k][1]))
    new_letters = set(min_word)
    new_letters.difference_update(fix_letters)
    max_value = angabe[min_word] - min_values_sum(min_word)
    return min_word, new_letters, max_value


# Function to calculate the sum of letters in a word
def word_sum(word, letter_values):
    return sum(letter_values[letter] for letter in word)


# Function to find letter values given words and their sums
def find_letter_values(words, fix):
    result = {letter: set() for letter in fix.keys()}
    # Generate all permutations of letter values
    for letter_values in get_all_permutations(fix):
        #print(f"letter_values: {letter_values}") # e. g. {'n': 3, 'g': 23, 'a': 20, 's': 14}
 
        # Check if the sum of each word matches the given sum
        if all(word_sum(word, letter_values) == angabe[word] for word in words):
            for letter, value in sorted(letter_values.items()):
                result[letter].add(value)
            #print(f"Letter values found: {result}")
    print("Result:")
    print(result)

    return result



# Example words and their sums
angabe = {"advent": 59, "bratapfel": 81, "christkind": 119, "engel": 20, "gans": 16, "heiligabend": 109, "jesus": 39, "kerzen": 35, "krippe": 61, "lametta": 42, "myrrhe": 70, "nikolaus": 81, "plaetzchen": 103, "quarkstollen": 111, "rentier": 28, "stern": 16, "tannenbaum": 69, "wunschzettel": 124, "xmas": 33, "zimtstern": 53}
#angabe = {"alaska": 37, "arizona": 59, "california": 90, "delaware": 60, "hawaii": 17, "idaho": 17, "iowa": 16, "maine": 43, "nebraska": 73, "nevada": 28, "new": 19, "jersey": 80, "ohio": 21, "oregon": 72, "pennsylvania": 96, "texas": 60, "utah": 55, "wyoming": 88}

fix = {}
words = []


# Find letter values
while len(fix) < 26:
    (next_word, next_letters, max_value) = get_next_best(angabe, fix)
    max_value = min(max_value, 26)
    print(f"next word: {next_word}, next_letters: {next_letters}, max_value: {max_value}")
    words.append(next_word)
    for letter in next_letters:
        fix[letter] = list(range(1, max_value + 1))
        print(f"fix: {fix}")
    fix = find_letter_values(words, fix)

print()
from pprint import pprint
pprint(fix)

#for word in angabe.keys():
#    c1, c2 = set(word), set(fix.keys())
#    c1.difference_update(c2)
#    print(word, c1)
